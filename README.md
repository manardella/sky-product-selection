# README #

## What is this repository for? ##

This repository is for Sky.com Java developer interview.

## Quick summary ##

This web application simultates a Product selection catalogue for Sky.com customers.


The application has 3 pages:

- index.xhtml -> Landing page.

- productselection.xhtml -> Page to select product and checkout (POST parameters to confirmation page).

- confirmationpage.xhtml -> Confirmation page with selected items.



The architecture is a MVC. Front-end consume REST services (sharing a model in json). The backend are services divided in logical layers. All classes are in the same project but are suitable for being moved to other projects for modularity (for example, package "services" can be moved to an EJB project). For data persistence the "DataModel.java" class simulates the persistance layer (no hibernate, jpa or unnecessary complexity for this test).



The application packages are:

- com.sky.productselection.beans -> Managed Beans for .xhtml (facelets)

- com.sky.productselection.constants -> Constants

- com.sky.productselection.context -> Class "AppServletContextListener" iniciates datamodel on serverlet load.

- com.sky.productselection.controller -> REST service (Jersey and Jackson API)

- com.sky.productselection.entity -> Entities

- com.sky.productselection.dal -> Data/Persistance layer (DataModel.java)

- com.sky.productselection.service -> Service interfaces and implementations

- com.sky.productselection.service.business -> Business logic

- com.sky.productselection.service.utils-> Utils

- com.sky.productselection.service.error -> Custom errors and exceptions




REST API (backend)

- The application has two services "customer-location" and "catalogue-service". All java classes are in **"com.sky.productselection.controller"**.

- This services are consumed by the front end scripts files. (./resources/js/*)



Web Client (frontend)

 - All files are under /src/main/java/webapp
 - * Some .js files in headers are downloaded from internet (Knockout.js, jquery and Bootstrap)



## How it works? ##

1. Browser -> Page -> JavaScript (jQuery, Knockout.js) -> Consumes REST Service -> Controller -> Controller Service -> Application Business logic -> Persistance layer (data model) ---> Returns JSON model data.

2. Browser manipulates JSON model and POST selected elements to confirmation page.
 


## How do I get set up? ##

- Clone the repository and run application WITH internet connection (for downloading maven dependencies and .js files).

### Steps ###

1. Clone the repository and import it in your IDE (NetBeans 8.1 and Eclipse Mars 2 has been tested).

2. Build project using MAVEN. The project requires JDK 7.

3. Deploy on application server Wildfly 10.0.0 or GlassFish 4.0.

4. Open in web browser (http://localhost:8080/SkyProductSelection/index.xhtml).

5. Navigate.

6. If you have problems loading productselection.xhtml at the first time, reload  the web page using Ctrl+F5. It happens because REST service is loading in the server side.

### Version ###
- Version 1.0

### Configuration ###
- No needs further configuration if you have Wildfly 10, GlassFish 4 and JDK 7.

### Dependencies ###
- JDK 7, Application Server, Jersey API, JUnit, log4java.
- * Some .js files in headers are downloaded from internet (Knockout.js, jquery and Bootstrap) on loading.

### Database configuration ###
- No required. A class "DataModel.java" simulates a persistence layer.

### How to run tests ###
- Run JUnit test files.

### Deployment instructions ###
- Compile with maven and move *.war file to deploy folder of your application server.