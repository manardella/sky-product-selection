/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.context;

import com.sky.web.productselection.dal.DataModel;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Context Listener for Servlet. Initiates de DataModel. Can be used to initiate
 * Hibernate, JPA, etc.. persistance layer.
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class AppServletContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {

        // Loading tmp data model for this application
        DataModel.getInstance().reload();

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }

}
