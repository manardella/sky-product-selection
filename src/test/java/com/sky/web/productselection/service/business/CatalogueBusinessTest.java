/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service.business;

import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CatalogueBusinessTest {

    public CatalogueBusinessTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCatalogueByLocation method, of class CatalogueBusiness.
     */
    @Test
    public void testGetCatalogueByLocation() throws Exception {
        System.out.println("getCatalogueByLocation");

        LocationBusiness locationBusiness = new LocationBusiness();
        CatalogueBusiness catalogueBusiness = new CatalogueBusiness();

        String locationID = "LONDON";

        Location location = locationBusiness.getLocationByLocationID(locationID);
        assertNotNull(location);
        assertEquals(locationID, location.getLocationID());

        List<Product> result = catalogueBusiness.getCatalogueByLocation(location);
        assertNotNull(result);
        assertTrue(!result.isEmpty());

        try {
            location = null;
            result = catalogueBusiness.getCatalogueByLocation(location);
        } catch (Exception e) {
            assertTrue(true);
        }

    }

}
