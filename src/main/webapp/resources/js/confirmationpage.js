/*
 * Javascript file for confirmationpage.xhtml
 */
$(document).ready(function () {
    //loadCheckoutInfoFromSession();    
    var selectedCustomer = JSON.parse($('#customerSelectedCatalogue').val()); //data posted in form 
    printUserBasketConfirmation(selectedCustomer);
});

/**
 * Loads confirmation page info from Session
 * @returns {undefined}
 */
function loadCheckoutInfoFromSession() {
    $('#customerID').val(getCustomerID());
    $('#locationID').val(getLocationID());
    $('#customerCatalogue').val(JSON.stringify(getCustomerCatalogue()));
    $('#customerSelectedCatalogue').val(JSON.stringify(getCustomerSelectedCatalogue()));
}

/**
 * Prints user basket
 * @param JSONObject selectedCatalogue
 * @returns {undefined}
 */
function printUserBasketConfirmation(selectedCatalogue) {
    $basket = $('#basket-confirm');
    $basket.html("");
    $.each(selectedCatalogue, function (idx, item) {
        var html = '<div>' + item.product + '<div>';
        $basket.append(html);
    });
}

