/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.service.business.LocationBusiness;

/**
 * Location service Implementation
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class LocationServiceImpl implements LocationService {

    LocationBusiness bs = new LocationBusiness();

    @Override
    public Location getLocationByCustomerID(String customerId) throws Exception {
        return bs.getLocationByCustomerID(customerId);
    }

    @Override
    public Location getLocationByLocationID(String locationID) throws Exception {
        return bs.getLocationByLocationID(locationID);
    }

}
