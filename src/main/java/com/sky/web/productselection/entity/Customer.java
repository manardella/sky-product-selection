/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@XmlRootElement
public class Customer {

    private String customerID;
    private String name;
    private String location;

    public Customer() {
    }

    public Customer(String customerID, String name, String locationID) {
        this.customerID = customerID;
        this.name = name;
        this.location = locationID;
    }

    @Override
    public String toString() {
        return "Customer{" + "customerID=" + customerID + ", name=" + name + ", location=" + location + '}';
    }

    /**
     * @return the customerID
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

}
