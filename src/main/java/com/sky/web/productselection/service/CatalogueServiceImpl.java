/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import com.sky.web.productselection.service.business.CatalogueBusiness;
import java.util.List;

/**
 * Implementation of Catalogue Service Interface
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CatalogueServiceImpl implements CatalogueService {

    CatalogueBusiness bs = new CatalogueBusiness();

    @Override
    public List<Product> getCatalogueByLocation(Location location) throws Exception {
        return bs.getCatalogueByLocation(location);
    }

}
