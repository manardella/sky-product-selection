/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.utils;

import java.lang.reflect.Field;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for java.lang.reflect utilities
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class ReflectionUtils {

    /**
     * Gets field value by .class() and field Name using reflection.
     *
     * @param clazz
     * @param fieldName
     * @return
     */
    public static Object getFieldValue(Class clazz, String fieldName) {
        Object value = null;

        Field f;
        try {
            f = clazz.getDeclaredField(fieldName);
            f.setAccessible(true);
            value = f.get(f.getType());
        } catch (NoSuchFieldException ex) {
            Logger.getLogger(ReflectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(ReflectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalArgumentException ex) {
            Logger.getLogger(ReflectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(ReflectionUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

        return value;
    }
}
