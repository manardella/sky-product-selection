/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.controller;

import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CustomerLocationControllerTest {

    public CustomerLocationControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLocationId method, of class CustomerLocationController.
     */
    @Test
    public void testGetLocationId() {
        System.out.println("getLocationId");
        String customerID = "1";

        CustomerLocationController instance = new CustomerLocationController();
        Response result = instance.getLocationId(customerID);
        assertTrue(result.getStatus() == 200);

        customerID = "noexists";
        result = instance.getLocationId(customerID);
        assertTrue(result.getStatus() == 404);

        customerID = null;
        result = instance.getLocationId(customerID);
        assertTrue(result.getStatus() == 404);

    }

}
