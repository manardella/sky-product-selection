/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.utils;

import com.sky.web.productselection.dal.DataModel;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class ReflectionUtilsTest {

    public ReflectionUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getFieldValue method, of class ReflectionUtils.
     */
    @Test
    public void testGetFieldValue() {
        System.out.println("getFieldValue");
        DataModel model = DataModel.getInstance();
        Class clazz = model.getClass();
        String fieldName = "INSTANCE";
        Object result = ReflectionUtils.getFieldValue(clazz, fieldName);
        assertNotNull(result);
    }

}
