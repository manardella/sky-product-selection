/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.error;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class FailureException extends Exception {

    public FailureException(String message) {
        super(message);
    }
}
