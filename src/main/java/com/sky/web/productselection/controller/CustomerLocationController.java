/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.controller;

import com.sky.web.productselection.constants.Errors;
import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.error.FailureException;
import com.sky.web.productselection.service.LocationService;
import com.sky.web.productselection.service.LocationServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Controller for REST service for customer-location
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@Path("customer-location")
public class CustomerLocationController {

    private static Logger logger = Logger.getLogger(CustomerLocationController.class.getName());

    /**
     * Method handling HTTP GET requests. The returned object will be sent to
     * the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    /*
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getIt() {
        return "location";
    }
     */
    @GET
    @Path("/customer/{customerID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getLocationId(@PathParam("customerID") String customerID) {

        Location location = null;
        try {

            LocationService srv = new LocationServiceImpl();
            location = srv.getLocationByCustomerID(customerID);

            if (location == null) {
                logger.log(Level.SEVERE, Errors.GET_CUSTOMER_LOCATION);
                return Response.status(404).entity(Errors.GET_CUSTOMER_LOCATION).build();
            }

        } catch (Exception ex) {

            FailureException customEx = new FailureException(Errors.GET_CUSTOMER_LOCATION);
            customEx.setStackTrace(ex.getStackTrace());

            logger.log(Level.SEVERE, Errors.GET_CUSTOMER_LOCATION, customEx);

            return Response.status(404).entity(customEx.getMessage()).build();
        }

        return Response.status(200).entity(location).build();
    }
}
