/*
 * Javascript file for productselection.xhtml
 */
$(document).ready(function() {
    loadCatalogueData();
});

/**
 * Loads POST data and navigate to confirmation page.
 * @returns {undefined}
 */
function navigateCheckoutJsf() {
    $('.customerID').val(getCustomerID());
    $('.locationID').val(getLocationID());
    $('.customerCatalogue').val(JSON.stringify(getCustomerCatalogue()));
    $('.customerSelectedCatalogue').val(JSON.stringify(getCustomerSelectedCatalogue()));
    $('.frmCheckoutSubmit').click();
}

/**
 * Uses Knockout.js . Loads catalogue and shows checkboxes
 * @param {type} selectedItem
 * @returns {Boolean}
 */
function toggleCatalogueSelectedElement(selectedItem) {
    var catalogue = getCustomerSelectedCatalogue();
    var index = -1;
    
    if (catalogue !== null  && catalogue !== undefined) {
        $.each(catalogue, function(idx, item) {
            if (item.productID == selectedItem.productID) {
                index = idx;
                return false;
            }
        });
        
        if (index > -1) {
            catalogue.splice(index, 1);
        } else {
            catalogue.push(selectedItem);
        }
    } else {
        catalogue = [];
        catalogue.push(selectedItem);
    }
    
    setCustomerSelectedCatalogue(catalogue);
    printUserBasket(catalogue);
    
    console.log(catalogue);
    return true;
}

/**
 * Prints user temporal basket
 * @param {type} catalogue
 * @returns {undefined}
 */
function printUserBasket(catalogue) {
    $basket = $('#basket');
    $basket.html("");
    $.each(catalogue, function(idx, item) {
        var html = '<div>' + item.product + '<div>';
        $basket.append(html);
    });
}

/**
 * Knockut.js model object for checkbox
 * @param JSONObject data item of catalogue
 * @returns {undefined}
 */
function ItemDataModel(data) {
    var self = this;
    self.data = data;
    self.product = data.product;
    self.checked = ko.observable(false);
    self.id = data.productID;
}

/**
 * Knockout.js model for product selection
 * @returns {undefined}
 */
function ProductSelectionModel() {
    var self = this;
    
    self.customerCatalogue = getCustomerCatalogue();
    
    var sportsArray = [];
    var newsArray = [];
    
    $.each(self.customerCatalogue, function(idx, item) {
        console.log(item.category);
        if (item.category == "Sports") {
            sportsArray.push(new ItemDataModel(item));
        } else {
            newsArray.push(new ItemDataModel(item));
        }
    });
    
    self.sports = ko.observableArray(sportsArray);
    self.news = ko.observableArray(newsArray);
    
    var updateSelectedProductsArray = function() {
        self.selectedProducts = ko.observableArray(getCustomerSelectedCatalogue());
        self.selectedProducts.valueHasMutated();
    }
    
    self.toggleProductSelection = function(itemCheckbox) {
        toggleCatalogueSelectedElement(itemCheckbox.data);
        return true;
    }
}

// knockout.js model initiation
ko.applyBindings(new ProductSelectionModel());
