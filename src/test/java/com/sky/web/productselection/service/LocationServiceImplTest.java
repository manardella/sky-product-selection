/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class LocationServiceImplTest {

    public LocationServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLocationByCustomerID method, of class LocationServiceImpl.
     */
    @Test
    public void testGetLocationByCustomerID() throws Exception {
        System.out.println("getLocationByCustomerID");

        LocationServiceImpl instance = new LocationServiceImpl();

        String customerId = "1";
        Location result = instance.getLocationByCustomerID(customerId);
        assertNotNull(result);

        customerId = "notexits";
        result = instance.getLocationByCustomerID(customerId);
        assertNull(result);

        customerId = null;
        result = instance.getLocationByCustomerID(customerId);
        assertNull(result);

    }

    /**
     * Test of getLocationByLocationID method, of class LocationServiceImpl.
     */
    @Test
    public void testGetLocationByLocationID() throws Exception {
        System.out.println("getLocationByLocationID");

        LocationServiceImpl instance = new LocationServiceImpl();

        String locationID = "LONDON";
        Location result = instance.getLocationByLocationID(locationID);
        assertNotNull(result);
        assertEquals(locationID, result.getLocationID());

        locationID = "noexits";
        result = instance.getLocationByLocationID(locationID);
        assertNull(result);

        locationID = null;
        result = instance.getLocationByLocationID(locationID);
        assertNull(result);

    }

}
