/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.beans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@ManagedBean
@Named(value = "confirmationPageBean")
@SessionScoped
public class ConfirmationPageBean {

    private String customerID;
    private String locationID;
    private String customerCatalogue;
    private String customerSelectedCatalogue;

    /**
     * Creates a new instance of ConfirmationPageBean
     */
    public ConfirmationPageBean() {
    }

    @PostConstruct
    public void loadPostParameters() {
        this.customerID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmCheckout:customerID");
        this.locationID = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmCheckout:locationID");
        this.customerCatalogue = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmCheckout:customerCatalogue");
        this.customerSelectedCatalogue = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("frmCheckout:customerSelectedCatalogue");
    }

    @Override
    public String toString() {
        return "ConfirmationPageBean{" + "customerID=" + customerID + ", locationID=" + locationID + ", customerCatalogue=" + customerCatalogue + ", customerSelectedCatalogue=" + customerSelectedCatalogue + '}';
    }

    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    /**
     * @return the customerCatalogue
     */
    public String getCustomerCatalogue() {
        return customerCatalogue;
    }

    /**
     * @param customerCatalogue the customerCatalogue to set
     */
    public void setCustomerCatalogue(String customerCatalogue) {
        this.customerCatalogue = customerCatalogue;
    }

    /**
     * @return the customerSelectedCatalogue
     */
    public String getCustomerSelectedCatalogue() {
        return customerSelectedCatalogue;
    }

    /**
     * @param customerSelectedCatalogue the customerSelectedCatalogue to set
     */
    public void setCustomerSelectedCatalogue(String customerSelectedCatalogue) {
        this.customerSelectedCatalogue = customerSelectedCatalogue;
    }

    /**
     * @return the customerID
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

}
