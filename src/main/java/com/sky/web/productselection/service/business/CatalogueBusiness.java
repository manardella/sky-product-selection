/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service.business;

import com.sky.web.productselection.dal.DataModel;
import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.List;

/**
 * Class for implement business logic. It can be places on other proyect or in
 * EJB module
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CatalogueBusiness {

    /**
     * Gets catalogue by location object
     *
     * @param location
     * @return
     * @throws Exception
     */
    public List<Product> getCatalogueByLocation(Location location) throws Exception {
        return DataModel.getInstance().getCatalogueByLocation(location);
    }

}
