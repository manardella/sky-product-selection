/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@XmlRootElement
public class Product {

    private int productID;
    private String category;
    private String product;
    private String location;

    public Product() {

    }

    @Override
    public String toString() {
        return "Product{" + "productID=" + productID + ", category=" + category + ", product=" + product + ", location=" + location + '}';
    }

    public Product(int productID, String category, String product, String location) {
        this.productID = productID;
        this.category = category;
        this.product = product;
        this.location = location;
    }

    /**
     * @return the productID
     */
    public int getProductID() {
        return productID;
    }

    /**
     * @param productID the productID to set
     */
    public void setProductID(int productID) {
        this.productID = productID;
    }

    /**
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category the category to set
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return the product
     */
    public String getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(String product) {
        this.product = product;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return location;
    }

    /**
     * @param location the location to set
     */
    public void setLocation(String location) {
        this.location = location;
    }

}
