/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.dal;

import com.sky.web.productselection.entity.Category;
import com.sky.web.productselection.entity.Customer;
import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.ArrayList;
import java.util.List;

/**
 * Data Model for temporal use
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class DataModel {

    private static final DataModel INSTANCE;

    List<Category> categories;
    List<Location> locations;
    List<Product> producs;
    List<Customer> customers;

    private DataModel() {

    }

    static {
        try {
            INSTANCE = new DataModel();
            INSTANCE.reload();
        } catch (Exception e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    /**
     * @return the instance
     */
    public static DataModel getInstance() {
        return INSTANCE;
    }

    public void reload() {

        // Clean
        this.locations = new ArrayList<>();
        this.producs = new ArrayList<>();
        this.customers = new ArrayList<>();

        // Locations
        this.locations.add(new Location("LONDON"));
        this.locations.add(new Location("LIVERPOOL"));

        // Products
        this.producs.add(new Product(0, "Sports", "Arsenal TV", "LONDON"));
        this.producs.add(new Product(1, "Sports", "Chelsea TV", "LONDON"));
        this.producs.add(new Product(2, "Sports", "Liverpool TV", "LIVERPOOL"));

        // Universal products
        this.producs.add(new Product(3, "News", "Sky News", null));
        this.producs.add(new Product(4, "News", "Sky Sports News", null));

        // Users. One for every location for testing
        this.customers.add(new Customer("1", "customer1", "LONDON"));
        this.customers.add(new Customer("2", "customer1", "LIVERPOOL"));

    }

    public Customer getCustomerById(String customerID) throws Exception {
        for (Customer c : customers) {
            if (c.getCustomerID().equalsIgnoreCase(customerID)) {
                return c;
            }
        }

        return null;
    }

    public Location getLocationByCustomerId(String customerID) throws Exception {
        Customer customer = getCustomerById(customerID);
        if (customer != null) {
            return getLocationById(customer.getLocation());
        }

        return null;
    }

    private Location getLocationById(String locationID) throws Exception {
        for (Location location : locations) {
            if (location.getLocationID().equalsIgnoreCase(locationID)) {
                return location;
            }
        }

        return null;
    }

    public List<Product> getCatalogueByLocation(Location location) throws Exception {
        List<Product> listProducts = new ArrayList<>();

        if (location == null) {
            throw new Exception("Location cannot be null!");
        }

        for (Product p : producs) {
            if (p.getLocation() == null) {
                listProducts.add(p);
            } else if (p.getLocation().equalsIgnoreCase(location.getLocationID())) {
                listProducts.add(p);
            }
        }

        return listProducts;
    }

    public Location getLocationByLocationID(String locationID) throws Exception {
        return getLocationById(locationID);
    }

}
