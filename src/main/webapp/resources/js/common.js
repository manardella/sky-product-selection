/*
 * Commons file for shared functions
 */

/**
 * Loads Catalogue from the server
 * @returns {undefined}
 */
function loadCatalogueData() {
    loadClientID();
}

/**
 * Prints customer info in the browser console.
 * @returns {undefined}
 */
function printCustomerInfo() {
    console.log(getCustomerID(), getLocationID(), getCustomerCatalogue());
}

/**
 * Loads catalogue using REST API
 * @returns {undefined}
 */
function loadCatalogue() {
    // rest service to load catalogue
    var catalogueServiceURL = "./webapi/catalogue-service/customer/location/{locationID}";
    var _url = catalogueServiceURL.replace("{locationID}", getLocationID());
    $.ajax({
        url: _url,
        type: 'get',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText);
            console.error('status loadCatalogue: ' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText, XMLHttpRequest);
        },
        success: function (data) {
            setCustomerCatalogue(data);
        }
    });
}

/**
 * Load location id using REST API
 * @returns {undefined}
 */
function loadLocationID() {
    // rest service to locate clientid 
    var customerLocationURL = "./webapi/customer-location/customer/{customerID}";
    var _url = customerLocationURL.replace("{customerID}", getCustomerID());
    $.ajax({
        url: _url,
        type: 'get',
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(XMLHttpRequest.responseText);
            console.error('status loadLocationID: ' + XMLHttpRequest.status + ', status text: ' + XMLHttpRequest.statusText, XMLHttpRequest);
        },
        success: function (data) {
            setLocationID(data.locationID)
            loadCatalogue();
        }
    });
}

/**
 * Loads clientID and calls to load location ID and catalogue
 * @returns {undefined}
 */
function loadClientID() {
    
    // Clean session data
    sessionStorage.removeItem("customerid");
    sessionStorage.removeItem("locationid");
    sessionStorage.removeItem("customercatalogue");
    sessionStorage.removeItem("customerselectedcatalogue");

    // User id by cookie
    var customerID = $.cookie('clientID');

    if (customerID === undefined) {
        console.warn("No existe la cookie 'customerID'");
        customerID = "1";
        $.cookie("customerID", customerID, {
            expires: 7
        });
    }

    setCustomerID(customerID);
    loadLocationID();
}

function setCustomerID(customerID) {
    sessionStorage.setItem("customerid", customerID);
}

function getCustomerID() {
    return sessionStorage.getItem("customerid");
}

function setLocationID(locationID) {
    sessionStorage.setItem("locationid", locationID);
}

function getLocationID(locationID) {
    return sessionStorage.getItem("locationid");
}

function setCustomerCatalogue(customerCatalogue) {
    sessionStorage.setItem("customercatalogue", JSON.stringify(customerCatalogue));
}

function getCustomerCatalogue() {
    return JSON.parse(sessionStorage.getItem("customercatalogue"));
}

function setCustomerSelectedCatalogue(customerSelectedCatalogue) {
    sessionStorage.setItem("customerselectedcatalogue", JSON.stringify(customerSelectedCatalogue));
}

function getCustomerSelectedCatalogue() {
    return JSON.parse(sessionStorage.getItem("customerselectedcatalogue"));
}




