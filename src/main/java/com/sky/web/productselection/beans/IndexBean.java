/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.beans;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@ManagedBean
@Named(value = "indexBean")
@RequestScoped
public class IndexBean {

    private String test;

    /**
     * Creates a new instance of IndexBean
     */
    public IndexBean() {
        this.test = "Test";
    }

    /**
     * @return the test
     */
    public String getTest() {
        return test;
    }

    /**
     * @param test the test to set
     */
    public void setTest(String test) {
        this.test = test;
    }

}
