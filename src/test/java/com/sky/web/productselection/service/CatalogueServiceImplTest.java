/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CatalogueServiceImplTest {

    public CatalogueServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCatalogueByLocation method, of class CatalogueServiceImpl.
     */
    @Test
    public void testGetCatalogueByLocation() throws Exception {
        System.out.println("getCatalogueByLocation");

        CatalogueServiceImpl instance = new CatalogueServiceImpl();
        LocationServiceImpl srv = new LocationServiceImpl();

        String locationID = "LONDON";
        Location location = srv.getLocationByLocationID(locationID);
        assertNotNull(location);
        List<Product> result = instance.getCatalogueByLocation(location);
        assertTrue(!result.isEmpty());

        try {
            locationID = "noexists";
            location = srv.getLocationByLocationID(locationID);
            assertNull(location);
            result = instance.getCatalogueByLocation(location);
            assertNull(result);

            locationID = null;
            location = srv.getLocationByLocationID(locationID);
            assertNull(location);
            result = instance.getCatalogueByLocation(location);
            assertNull(result);

        } catch (Exception e) {
            assertTrue(true);
        }

    }

}
