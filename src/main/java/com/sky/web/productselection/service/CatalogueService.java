/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.List;

/**
 * Interface for catalogue-services
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public interface CatalogueService extends Service {

    /**
     * Gets catalogue by Location object
     *
     * @param location
     * @return
     * @throws Exception
     */
    public List<Product> getCatalogueByLocation(Location location) throws Exception;

}
