/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.context;

import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class AppServletContextListenerTest {

    public AppServletContextListenerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of contextInitialized method, of class AppServletContextListener.
     */
    @Test
    public void testContextInitialized() {
        System.out.println("contextInitialized");
        assertTrue(true);
    }

    /**
     * Test of contextDestroyed method, of class AppServletContextListener.
     */
    @Test
    public void testContextDestroyed() {
        System.out.println("contextDestroyed");
        assertTrue(true);
    }

}
