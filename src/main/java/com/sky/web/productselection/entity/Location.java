/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 * @XmlTransient
 */
@XmlRootElement
public class Location {

    private String locationID;

    public Location() {

    }

    @Override
    public String toString() {
        return "Location{" + "locationID=" + locationID + '}';
    }

    public Location(String locationID) {
        this.locationID = locationID;
    }

    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

}
