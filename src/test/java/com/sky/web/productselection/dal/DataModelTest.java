/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.dal;

import com.sky.web.productselection.dal.DataModel;
import com.sky.web.productselection.entity.Customer;
import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class DataModelTest {

    public DataModelTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class DataModel.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        DataModel result = DataModel.getInstance();
        assertNotNull(result);
    }

    /**
     * Test of reload method, of class DataModel.
     */
    @Test
    public void testReload() {
        System.out.println("reload");
        DataModel instance = DataModel.getInstance();
        instance.reload();
        assert (true);
    }

    /**
     * Test of getCustomerById method, of class DataModel.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCustomerById() throws Exception {
        System.out.println("getCustomerById");

        String customerID = "1";
        DataModel instance = DataModel.getInstance();
        Customer result = instance.getCustomerById(customerID);
        assertNotNull(result);

        customerID = "noexists";
        result = instance.getCustomerById(customerID);
        assertNull(result);

        customerID = null;
        result = instance.getCustomerById(customerID);
        assertNull(result);

    }

    /**
     * Test of getLocationByCustomerId method, of class DataModel.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetLocationByCustomerId() throws Exception {
        System.out.println("getLocationByCustomerId");

        String customerID = "1";
        DataModel instance = DataModel.getInstance();
        Location result = instance.getLocationByCustomerId(customerID);
        assertNotNull(result);

        customerID = "noexits";
        result = instance.getLocationByCustomerId(customerID);
        assertNull(result);

        customerID = null;
        result = instance.getLocationByCustomerId(customerID);
        assertNull(result);
    }

    /**
     * Test of getCatalogueByLocation method, of class DataModel.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testGetCatalogueByLocation() throws Exception {
        System.out.println("getCatalogueByLocation");
        DataModel instance = DataModel.getInstance();

        Location location = instance.getLocationByLocationID("LONDON");
        assertNotNull(location);
        List<Product> result = instance.getCatalogueByLocation(location);
        assertNotNull(result);
        assertTrue(!result.isEmpty()); //always global products must be presented
    }

    /**
     * Test of getLocationByLocationID method, of class DataModel.
     */
    @Test
    public void testGetLocationByLocationID() throws Exception {
        System.out.println("getLocationByLocationID");

        DataModel instance = DataModel.getInstance();
        String locationID = "LONDON";
        String expResult = "LONDON";
        Location result = instance.getLocationByLocationID(locationID);
        assertEquals(expResult, result.getLocationID());

        locationID = "noexists";
        result = instance.getLocationByLocationID(locationID);
        assertNull(result);

        locationID = null;
        result = instance.getLocationByLocationID(locationID);
        assertNull(result);

    }

}
