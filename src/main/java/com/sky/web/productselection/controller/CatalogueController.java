package com.sky.web.productselection.controller;

import com.sky.web.productselection.constants.Errors;
import com.sky.web.productselection.entity.Location;
import com.sky.web.productselection.entity.Product;
import com.sky.web.productselection.error.FailureException;
import com.sky.web.productselection.service.CatalogueService;
import com.sky.web.productselection.service.CatalogueServiceImpl;
import com.sky.web.productselection.service.LocationService;
import com.sky.web.productselection.service.LocationServiceImpl;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Controller for REST service for catalogue-service
 */
@Path("catalogue-service")
public class CatalogueController {

    private static final Logger logger = Logger.getLogger(CatalogueController.class.getName());

    /**
     * Method handling HTTP GET requests. The returned object will be sent to
     * the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Path("/customer/location/{locationID}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCatalogueByLocationId(@PathParam("locationID") String locationID) {

        List<Product> catalogue;
        try {

            LocationService locationService = new LocationServiceImpl();
            Location location = locationService.getLocationByLocationID(locationID);

            if (location == null) {
                logger.log(Level.SEVERE, Errors.GET_CUSTOMER_LOCATION);
                return Response.status(404).entity(Errors.GET_CUSTOMER_LOCATION).build();
            }

            CatalogueService srv = new CatalogueServiceImpl();
            catalogue = srv.getCatalogueByLocation(location);

        } catch (Exception ex) {

            FailureException customEx = new FailureException(Errors.GET_CATALOGUE_LOCATION);
            customEx.setStackTrace(ex.getStackTrace());
            logger.log(Level.SEVERE, Errors.GET_CATALOGUE_LOCATION, customEx);

            return Response.status(404).entity(customEx.getMessage()).build();
        }

        //GenericEntity<List<Product>> list = new GenericEntity<List<Product>>(catalogue) {
        //};
        return Response.status(200).entity(catalogue).build();

    }
}
