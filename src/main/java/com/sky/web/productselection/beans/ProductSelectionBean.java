/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
@ManagedBean
@Named(value = "productSelectionBean")
@SessionScoped
public class ProductSelectionBean {

    private String customerID;
    private String locationID;
    private String customerCatalogue;
    private String customerSelectedCatalogue;

    /**
     * Creates a new instance of ProductSelectionBean
     */
    public ProductSelectionBean() {
    }

    @Override
    public String toString() {
        return "ProductSelectionBean{" + "customerID=" + customerID + ", locationID=" + locationID + ", customerCatalogue=" + customerCatalogue + ", customerSelectedCatalogue=" + customerSelectedCatalogue + '}';
    }

    /**
     * @return the customerID
     */
    public String getCustomerID() {
        return customerID;
    }

    /**
     * @param customerID the customerID to set
     */
    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    /**
     * @return the locationID
     */
    public String getLocationID() {
        return locationID;
    }

    /**
     * @param locationID the locationID to set
     */
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    /**
     * @return the customerCatalogue
     */
    public String getCustomerCatalogue() {
        return customerCatalogue;
    }

    /**
     * @param customerCatalogue the customerCatalogue to set
     */
    public void setCustomerCatalogue(String customerCatalogue) {
        this.customerCatalogue = customerCatalogue;
    }

    /**
     * @return the customerSelectedCatalogue
     */
    public String getCustomerSelectedCatalogue() {
        return customerSelectedCatalogue;
    }

    /**
     * @param customerSelectedCatalogue the customerSelectedCatalogue to set
     */
    public void setCustomerSelectedCatalogue(String customerSelectedCatalogue) {
        this.customerSelectedCatalogue = customerSelectedCatalogue;
    }

}
