/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service;

import com.sky.web.productselection.entity.Location;

/**
 * Location service interface
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public interface LocationService extends Service {

    /**
     * Get Location object by customerID
     *
     * @param customerId
     * @return
     * @throws Exception
     */
    public Location getLocationByCustomerID(String customerId) throws Exception;

    /**
     * Gets Location Object by locationID
     *
     * @param locationID
     * @return
     * @throws Exception
     */
    public Location getLocationByLocationID(String locationID) throws Exception;

}
