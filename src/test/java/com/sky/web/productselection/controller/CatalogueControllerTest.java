/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.controller;

import javax.ws.rs.core.Response;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class CatalogueControllerTest {

    public CatalogueControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getCatalogueByLocationId method, of class CatalogueController.
     */
    @Test
    public void testGetCatalogueByLocationId() {
        System.out.println("getCatalogueByLocationId");
        CatalogueController instance = new CatalogueController();

        String locationID = "LONDON";
        Response result = instance.getCatalogueByLocationId(locationID);
        assertTrue(result.getStatus() == 200);

        locationID = null;
        result = instance.getCatalogueByLocationId(locationID);
        assertTrue(result.getStatus() == 404);

    }

}
