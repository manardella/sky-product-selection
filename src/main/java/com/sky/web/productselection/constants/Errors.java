/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.constants;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public final class Errors {

    public static final String GET_CUSTOMER_LOCATION = "There was a problem retrieving the customer information.";

    public static final String GET_CATALOGUE_LOCATION = "There was a problem retrieving the customer catalogue information.";
}
