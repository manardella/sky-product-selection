/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.constants;

/**
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public final class Results {

    // Service Result
    public static final int RESULT_OK = 0;
    public static final String RESULT_OK_MSG = "RESULT_OK_MSG";

    public static final int RESULT_NOK = 1;
    public static final String RESULT_NOK_MSG = "RESULT_NOK_MSG";

    public static final int RESULT_NOK_OPERATION_DO_NOT_EXITS = 1001;
    public static final String RESULT_NOK_OPERATION_DO_NOT_EXITS_MSG = "RESULT_NOK_OPERATION_DO_NOT_EXITS_MSG";

}
