/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sky.web.productselection.service.business;

import com.sky.web.productselection.dal.DataModel;
import com.sky.web.productselection.entity.Location;

/**
 * Class for implement business logic. It can be places on other proyect or in
 * EJB module
 *
 * @author Martin Nardella <martin.nardella@hotmail.com>
 */
public class LocationBusiness {

    /**
     * Gets location by CustomerID
     *
     * @param customerId
     * @return
     * @throws Exception
     */
    public Location getLocationByCustomerID(String customerId) throws Exception {
        return DataModel.getInstance().getLocationByCustomerId(customerId);
    }

    /**
     * Gets location by locationID
     *
     * @param locationID
     * @return
     * @throws Exception
     */
    public Location getLocationByLocationID(String locationID) throws Exception {
        return DataModel.getInstance().getLocationByLocationID(locationID);
    }

}
